QT += core gui opengl xml widgets
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG += qt
CONFIG +=c++11


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

SOURCES += main.cpp \
    loader.cpp \
    querymanager.cpp \
    dbwindow.cpp \
    createcontainer.cpp \
    treeitem.cpp \
    treemodel.cpp \
    treeresults.cpp \


HEADERS += \
    loader.h \
    querymanager.h \
    dbwindow.h \
    createcontainer.h \
    treeitem.h \
    treemodel.h \
    treeresults.h

FORMS += \
    dbwindow.ui \
    createcontainer.ui \
    treeresults.ui


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../dbxml-6.0.18/lib/ -llibdb61 -llibdbxml60 -lxerces-c_3 -lxqilla23
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../dbxml-6.0.18/lib/ -llibdb61d -llibdbxml60 -lxerces-c_3D -lxqilla23d

INCLUDEPATH += $$PWD/../../../../../dbxml-6.0.18/include
DEPENDPATH += $$PWD/../../../../../dbxml-6.0.18/include
