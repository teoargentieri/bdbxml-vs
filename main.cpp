#include <iostream>
#include <fstream>
#include <dbxml/DbXml.hpp>
#include <db.h>
//#include <dirent.h>
#include "loader.h"
#include "querymanager.h"
#include "dbwindow.h"
#include <QApplication>
#include <sys/stat.h>
#include <string>
#include <stdio.h>
#include<QStandardItemModel>
#include <QModelIndex>
#include <QVariant>
#include "treemodel.h"
#include <QTreeView>

using namespace DbXml;
/*
int checkDirectory(const char* path){
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (path)) != NULL) {
      // print all the files and directories within directory
      while ((ent = readdir (dir)) != NULL) {
         std::string nome=ent->d_name;
         std::string filePath=path;
         filePath+="/"+nome;
         std::string format=nome.substr(nome.find_last_of("/")+1);
         std::cout<<filePath+"\n";
      }
      closedir (dir);
    } else {
      // could not open directory
      perror ("");
      return -1;
    }
}*/

inline bool exists_test (const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}


int main(int argc, char *argv[])
{
    QApplication a(argc,argv);
    QCoreApplication::setApplicationName("Database Test");
    DbWindow dbw;

    dbw.show();

    return a.exec();

}

