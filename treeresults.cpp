#include "treeresults.h"
#include "ui_treeresults.h"
#include "treeitem.h"

TreeResults::TreeResults(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TreeResults),
    dir("save_report")
{
    ui->setupUi(this);
    treeView=ui->treeView;
    this->setFixedSize(this->size());
}

TreeResults::~TreeResults()
{
    delete ui;
}

void TreeResults::setModelView(TreeModel* model){
    treeView->setModel(model);
    treeView->repaint();
    treeModel=model;

}

void TreeResults::on_actionSave_triggered()
{
    checkDir();
    QFile file(QString::fromStdString(dir.toStdString()+"/"+treeModel->getProtein()+".txt"));
    if (file.open(QIODevice::WriteOnly)) {
        file.write(treeModel->getFile());
        file.close();
    }
}

void TreeResults::on_actionSave_as_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Save Report file"), "",
        tr("Report (*.txt);;All Files (*)"));

    if (fileName.isEmpty())
        return;
    else {
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly)) {
            QMessageBox::information(this, tr("Unable to open file"),
                file.errorString());
            return;
        }else{
            file.write(treeModel->getFile());
            file.close();
        }
}
}
