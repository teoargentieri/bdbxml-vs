#include "createcontainer.h"
#include "ui_createcontainer.h"

CreateContainer::CreateContainer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateContainer),active(false)
{
    ui->setupUi(this);
    bb=ui->buttonBox;
}

CreateContainer::~CreateContainer()
{
    delete ui;
}

void CreateContainer::on_buttonBox_accepted()
{
    nuovoContainer=ui->plainTextEdit->toPlainText().toStdString();
    nuovoContainer+=".dbxml";
    emit mySignal();
    this->setHidden(true);
    this->~CreateContainer();
}

void CreateContainer::on_pushButton_clicked()
{
    this->~CreateContainer();
}
