/********************************************************************************
** Form generated from reading UI file 'treeresults.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TREERESULTS_H
#define UI_TREERESULTS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TreeResults
{
public:
    QAction *actionSave;
    QAction *actionSave_as;
    QWidget *centralwidget;
    QTreeView *treeView;
    QMenuBar *menubar;
    QMenu *menuReport;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *TreeResults)
    {
        if (TreeResults->objectName().isEmpty())
            TreeResults->setObjectName(QStringLiteral("TreeResults"));
        TreeResults->resize(696, 408);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TreeResults->sizePolicy().hasHeightForWidth());
        TreeResults->setSizePolicy(sizePolicy);
        actionSave = new QAction(TreeResults);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionSave_as = new QAction(TreeResults);
        actionSave_as->setObjectName(QStringLiteral("actionSave_as"));
        centralwidget = new QWidget(TreeResults);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        treeView = new QTreeView(centralwidget);
        treeView->setObjectName(QStringLiteral("treeView"));
        treeView->setGeometry(QRect(0, 0, 701, 361));
        treeView->header()->setHighlightSections(true);
        TreeResults->setCentralWidget(centralwidget);
        menubar = new QMenuBar(TreeResults);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 696, 19));
        menuReport = new QMenu(menubar);
        menuReport->setObjectName(QStringLiteral("menuReport"));
        TreeResults->setMenuBar(menubar);
        statusbar = new QStatusBar(TreeResults);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        TreeResults->setStatusBar(statusbar);

        menubar->addAction(menuReport->menuAction());
        menuReport->addAction(actionSave);
        menuReport->addAction(actionSave_as);

        retranslateUi(TreeResults);

        QMetaObject::connectSlotsByName(TreeResults);
    } // setupUi

    void retranslateUi(QMainWindow *TreeResults)
    {
        TreeResults->setWindowTitle(QApplication::translate("TreeResults", "MainWindow", 0));
        actionSave->setText(QApplication::translate("TreeResults", "Save", 0));
        actionSave_as->setText(QApplication::translate("TreeResults", "Save as ...", 0));
        menuReport->setTitle(QApplication::translate("TreeResults", "Report", 0));
    } // retranslateUi

};

namespace Ui {
    class TreeResults: public Ui_TreeResults {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TREERESULTS_H
