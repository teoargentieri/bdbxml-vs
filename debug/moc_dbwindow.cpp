/****************************************************************************
** Meta object code from reading C++ file 'dbwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.2.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../dbwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dbwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.2.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_DbWindow_t {
    QByteArrayData data[19];
    char stringdata[435];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_DbWindow_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_DbWindow_t qt_meta_stringdata_DbWindow = {
    {
QT_MOC_LITERAL(0, 0, 8),
QT_MOC_LITERAL(1, 9, 35),
QT_MOC_LITERAL(2, 45, 0),
QT_MOC_LITERAL(3, 46, 34),
QT_MOC_LITERAL(4, 81, 27),
QT_MOC_LITERAL(5, 109, 8),
QT_MOC_LITERAL(6, 118, 10),
QT_MOC_LITERAL(7, 129, 32),
QT_MOC_LITERAL(8, 162, 27),
QT_MOC_LITERAL(9, 190, 5),
QT_MOC_LITERAL(10, 196, 35),
QT_MOC_LITERAL(11, 232, 21),
QT_MOC_LITERAL(12, 254, 21),
QT_MOC_LITERAL(13, 276, 22),
QT_MOC_LITERAL(14, 299, 21),
QT_MOC_LITERAL(15, 321, 4),
QT_MOC_LITERAL(16, 326, 34),
QT_MOC_LITERAL(17, 361, 29),
QT_MOC_LITERAL(18, 391, 42)
    },
    "DbWindow\0on_actionChoose_directory_triggered\0"
    "\0on_actionChoose_document_triggered\0"
    "on_DbWindow_iconSizeChanged\0iconSize\0"
    "mostranomi\0on_actionSet_Container_triggered\0"
    "on_listWidget_doubleClicked\0index\0"
    "on_actionCreate_Container_triggered\0"
    "createContainerAction\0on_listWidget_clicked\0"
    "on_startButton_clicked\0on_comboBox_activated\0"
    "arg1\0on_actionSource_proteins_triggered\0"
    "on_actionSerach_all_triggered\0"
    "on_actionClear_previous_research_triggered\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DbWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   84,    2, 0x08,
       3,    0,   85,    2, 0x08,
       4,    1,   86,    2, 0x08,
       6,    0,   89,    2, 0x08,
       7,    0,   90,    2, 0x08,
       8,    1,   91,    2, 0x08,
      10,    0,   94,    2, 0x08,
      11,    0,   95,    2, 0x08,
      12,    1,   96,    2, 0x08,
      13,    0,   99,    2, 0x08,
      14,    1,  100,    2, 0x08,
      16,    0,  103,    2, 0x08,
      17,    0,  104,    2, 0x08,
      18,    0,  105,    2, 0x08,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QSize,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    9,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void DbWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        DbWindow *_t = static_cast<DbWindow *>(_o);
        switch (_id) {
        case 0: _t->on_actionChoose_directory_triggered(); break;
        case 1: _t->on_actionChoose_document_triggered(); break;
        case 2: _t->on_DbWindow_iconSizeChanged((*reinterpret_cast< const QSize(*)>(_a[1]))); break;
        case 3: _t->mostranomi(); break;
        case 4: _t->on_actionSet_Container_triggered(); break;
        case 5: _t->on_listWidget_doubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 6: _t->on_actionCreate_Container_triggered(); break;
        case 7: _t->createContainerAction(); break;
        case 8: _t->on_listWidget_clicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 9: _t->on_startButton_clicked(); break;
        case 10: _t->on_comboBox_activated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 11: _t->on_actionSource_proteins_triggered(); break;
        case 12: _t->on_actionSerach_all_triggered(); break;
        case 13: _t->on_actionClear_previous_research_triggered(); break;
        default: ;
        }
    }
}

const QMetaObject DbWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_DbWindow.data,
      qt_meta_data_DbWindow,  qt_static_metacall, 0, 0}
};


const QMetaObject *DbWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DbWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DbWindow.stringdata))
        return static_cast<void*>(const_cast< DbWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int DbWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
