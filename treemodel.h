
#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
#include <string>
#include<iostream>
#include"querymanager.h"
#include"db.h"
#include"dbxml/DbXml.hpp"


class TreeItem;

class TreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit TreeModel(std::string nomeContainer,int cathegoryValue,std::string protein,QObject *parent = 0);
    ~TreeModel();

    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    TreeItem *getRoot();

    QByteArray getFile(){
        return file;
    }

    std::string getProtein(){
        return protein;
    }


private:
    void setupModelData(std::string nomeContainer,TreeItem *parent);

    TreeItem *rootItem;
    int cathegoryValue;
    QByteArray file;
    std::string protein;

};

#endif // TREEMODEL_H
