#ifndef LOADER_H
#define LOADER_H
#include <dbxml/DbXml.hpp>
#include <db.h>

class Loader
{
public:

//attributes
DbXml::XmlManager manager;
DbXml::XmlContainer container;
DbXml::XmlUpdateContext context ;

bool caricamento;

//metods
    Loader();
    Loader(const char* containerName);
    void setContainer(std::string containerName);
    void    loadDataFromFile(const char* filePath,const char* nomeDoc);
    void    loadDataFromFile(const char* filePath);
    void    loadDataFromDirectory(const char* directoryPath);
    void loadFromDoc(DbXml::XmlDocument doc);
    void loadFromString(std::string docname,std::string content);
};

#endif // LOADER_H
