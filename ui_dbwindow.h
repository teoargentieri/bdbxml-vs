/********************************************************************************
** Form generated from reading UI file 'dbwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DBWINDOW_H
#define UI_DBWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DbWindow
{
public:
    QAction *actionChoose_directory;
    QAction *actionChoose_document;
    QAction *actionSet_Container;
    QAction *actionCreate_Container;
    QAction *actionSource_proteins;
    QAction *actionSerach_all;
    QAction *actionClear_previous_research;
    QWidget *central;
    QFormLayout *formLayout;
    QLabel *label;
    QListWidget *listWidget;
    QTextBrowser *viewArea;
    QHBoxLayout *horizontalLayout;
    QLabel *psLabelDescription;
    QLabel *psLabel;
    QPushButton *startButton;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_2;
    QComboBox *comboBox;
    QMenuBar *menubar;
    QMenu *menuLoad;
    QMenu *menuQuery;

    void setupUi(QMainWindow *DbWindow)
    {
        if (DbWindow->objectName().isEmpty())
            DbWindow->setObjectName(QStringLiteral("DbWindow"));
        DbWindow->resize(693, 324);
        DbWindow->setCursor(QCursor(Qt::PointingHandCursor));
        DbWindow->setToolTipDuration(-4);
        DbWindow->setAutoFillBackground(true);
        actionChoose_directory = new QAction(DbWindow);
        actionChoose_directory->setObjectName(QStringLiteral("actionChoose_directory"));
        actionChoose_document = new QAction(DbWindow);
        actionChoose_document->setObjectName(QStringLiteral("actionChoose_document"));
        actionSet_Container = new QAction(DbWindow);
        actionSet_Container->setObjectName(QStringLiteral("actionSet_Container"));
        actionCreate_Container = new QAction(DbWindow);
        actionCreate_Container->setObjectName(QStringLiteral("actionCreate_Container"));
        actionSource_proteins = new QAction(DbWindow);
        actionSource_proteins->setObjectName(QStringLiteral("actionSource_proteins"));
        actionSerach_all = new QAction(DbWindow);
        actionSerach_all->setObjectName(QStringLiteral("actionSerach_all"));
        actionClear_previous_research = new QAction(DbWindow);
        actionClear_previous_research->setObjectName(QStringLiteral("actionClear_previous_research"));
        central = new QWidget(DbWindow);
        central->setObjectName(QStringLiteral("central"));
        central->setAutoFillBackground(true);
        formLayout = new QFormLayout(central);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setSizeConstraint(QLayout::SetMaximumSize);
        formLayout->setFieldGrowthPolicy(QFormLayout::ExpandingFieldsGrow);
        label = new QLabel(central);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label);

        listWidget = new QListWidget(central);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setAutoFillBackground(true);

        formLayout->setWidget(4, QFormLayout::LabelRole, listWidget);

        viewArea = new QTextBrowser(central);
        viewArea->setObjectName(QStringLiteral("viewArea"));
        viewArea->setAutoFillBackground(true);

        formLayout->setWidget(4, QFormLayout::FieldRole, viewArea);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        psLabelDescription = new QLabel(central);
        psLabelDescription->setObjectName(QStringLiteral("psLabelDescription"));
        psLabelDescription->setEnabled(false);

        horizontalLayout->addWidget(psLabelDescription);

        psLabel = new QLabel(central);
        psLabel->setObjectName(QStringLiteral("psLabel"));

        horizontalLayout->addWidget(psLabel);


        formLayout->setLayout(6, QFormLayout::LabelRole, horizontalLayout);

        startButton = new QPushButton(central);
        startButton->setObjectName(QStringLiteral("startButton"));
        startButton->setEnabled(false);

        formLayout->setWidget(6, QFormLayout::FieldRole, startButton);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_2 = new QLabel(central);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_3->addWidget(label_2);

        comboBox = new QComboBox(central);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setMinimumSize(QSize(100, 0));

        horizontalLayout_3->addWidget(comboBox);


        formLayout->setLayout(1, QFormLayout::FieldRole, horizontalLayout_3);

        DbWindow->setCentralWidget(central);
        menubar = new QMenuBar(DbWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 693, 21));
        menuLoad = new QMenu(menubar);
        menuLoad->setObjectName(QStringLiteral("menuLoad"));
        menuQuery = new QMenu(menubar);
        menuQuery->setObjectName(QStringLiteral("menuQuery"));
        DbWindow->setMenuBar(menubar);

        menubar->addAction(menuLoad->menuAction());
        menubar->addAction(menuQuery->menuAction());
        menuLoad->addAction(actionChoose_directory);
        menuLoad->addAction(actionChoose_document);
        menuLoad->addAction(actionSet_Container);
        menuLoad->addAction(actionCreate_Container);
        menuQuery->addAction(actionSource_proteins);
        menuQuery->addAction(actionSerach_all);
        menuQuery->addAction(actionClear_previous_research);

        retranslateUi(DbWindow);

        QMetaObject::connectSlotsByName(DbWindow);
    } // setupUi

    void retranslateUi(QMainWindow *DbWindow)
    {
        DbWindow->setWindowTitle(QApplication::translate("DbWindow", "Database Test", 0));
#ifndef QT_NO_WHATSTHIS
        DbWindow->setWhatsThis(QApplication::translate("DbWindow", "<html><head/><body><p><br/></p></body></html>", 0));
#endif // QT_NO_WHATSTHIS
        actionChoose_directory->setText(QApplication::translate("DbWindow", "Choose Directory", 0));
        actionChoose_document->setText(QApplication::translate("DbWindow", "Choose Document", 0));
        actionSet_Container->setText(QApplication::translate("DbWindow", "Set Container", 0));
        actionCreate_Container->setText(QApplication::translate("DbWindow", "Create Container", 0));
        actionSource_proteins->setText(QApplication::translate("DbWindow", "Source proteins", 0));
        actionSerach_all->setText(QApplication::translate("DbWindow", "Serach all", 0));
        actionClear_previous_research->setText(QApplication::translate("DbWindow", "Clear previous research", 0));
        label->setText(QApplication::translate("DbWindow", "Report stored in Container", 0));
        psLabelDescription->setText(QApplication::translate("DbWindow", "Source Protein", 0));
        psLabel->setText(QString());
        startButton->setText(QApplication::translate("DbWindow", "Motif Search", 0));
        label_2->setText(QApplication::translate("DbWindow", "Common Category:   ", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("DbWindow", "Superfamily", 0)
         << QApplication::translate("DbWindow", "Fold", 0)
         << QApplication::translate("DbWindow", "Class", 0)
        );
        menuLoad->setTitle(QApplication::translate("DbWindow", "Load", 0));
        menuQuery->setTitle(QApplication::translate("DbWindow", "Query", 0));
    } // retranslateUi

};

namespace Ui {
    class DbWindow: public Ui_DbWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DBWINDOW_H
