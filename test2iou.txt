<test protein="2iou">
	<motif catenaSource="6" idSource="16 20 23" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="13 14 16"/>
	</motif> 
	<motif catenaSource="6" idSource="1 4 6" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="13 14 16"/>
	</motif> 
	<motif catenaSource="6" idSource="13 16 20" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="13 14 16"/>
	</motif> 
	<motif catenaSource="7" idSource="4 6 12" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="11 13 14"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="11 13 16"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="11 13 16"/>
	</motif> 
	<motif catenaSource="7" idSource="6 12 14" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="11 14 16"/>
	</motif> 
	<motif catenaSource="7" idSource="4 9 12" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="11 14 16"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="13 14 16"/>
	</motif> 
	<motif catenaSource="7" idSource="4 6 9" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="13 14 16"/>
	</motif> 
	<motif catenaSource="7" idSource="1 4 6" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="13 14 16"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="11 16 18"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 13" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="11 13 16"/>
	</motif> 
	<motif catenaSource="9" idSource="1 4 6" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="11 13 16"/>
	</motif> 
	<motif catenaSource="9" idSource="6 11 13" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="11 14 16"/>
	</motif> 
	<motif catenaSource="9" idSource="4 9 11" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="11 14 16"/>
	</motif> 
	<motif catenaSource="9" idSource="4 6 11" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="11 14 16"/>
	</motif> 
	<motif catenaSource="9" idSource="1 4 6" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="13 14 16"/>
		<proteina name="1e8c" catenaSearch="0" idSearch="33 35 40"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="1" idSearch="11 16 18"/>
	</motif> 
	<motif catenaSource="9" idSource="1 4 6" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="1" idSearch="33 35 39"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="0" idSearch="1 2 3"/>
	</motif> 
	<motif catenaSource="7" idSource="1 4 6" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="0" idSearch="18 19 20"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="0" idSearch="18 19 20"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 13" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="0" idSearch="1 2 3"/>
	</motif> 
	<motif catenaSource="9" idSource="1 4 11" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="0" idSearch="35 41 42"/>
	</motif> 
	<motif catenaSource="9" idSource="0 3 17" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="0" idSearch="34 43 44"/>
	</motif> 
	<motif catenaSource="6" idSource="13 16 20" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="3" idSearch="9 10 11"/>
	</motif> 
	<motif catenaSource="6" idSource="1 4 6" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="3" idSearch="9 10 11"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="3" idSearch="1 2 3"/>
	</motif> 
	<motif catenaSource="7" idSource="4 6 9" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="3" idSearch="9 10 11"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="3" idSearch="9 10 11"/>
	</motif> 
	<motif catenaSource="7" idSource="4 14 16" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="3" idSearch="3 4 18"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 13" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="3" idSearch="1 2 3"/>
	</motif> 
	<motif catenaSource="9" idSource="1 9 15" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="3" idSearch="35 39 58"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 14" numberItems="3" > 
		<proteina name="2bcc" catenaSearch="0" idSearch="1 2 3"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="2bcc" catenaSearch="0" idSearch="2 3 16"/>
	</motif> 
	<motif catenaSource="7" idSource="4 6 14" numberItems="3" > 
		<proteina name="2bcc" catenaSearch="0" idSearch="2 7 16"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 13" numberItems="3" > 
		<proteina name="2bcc" catenaSearch="0" idSearch="2 3 16"/>
	</motif> 
	<motif catenaSource="9" idSource="4 11 13" numberItems="3" > 
		<proteina name="2bcc" catenaSearch="0" idSearch="2 7 16"/>
	</motif> 
	<motif catenaSource="9" idSource="4 6 13" numberItems="3" > 
		<proteina name="2bcc" catenaSearch="0" idSearch="2 7 16"/>
	</motif> 
	<motif catenaSource="7" idSource="12 14 16" numberItems="3" > 
		<proteina name="2bcc" catenaSearch="1" idSearch="0 6 14"/>
	</motif> 
	<motif catenaSource="9" idSource="11 13 15" numberItems="3" > 
		<proteina name="2bcc" catenaSearch="1" idSearch="0 6 14"/>
	</motif> 
	<motif catenaSource="9" idSource="12 14 21" numberItems="3" > 
		<proteina name="2euh" catenaSearch="0" idSearch="33 36 38"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="2euh" catenaSearch="1" idSearch="21 36 38"/>
	</motif> 
	<motif catenaSource="9" idSource="12 14 21" numberItems="3" > 
		<proteina name="2euh" catenaSearch="1" idSearch="33 36 38"/>
	</motif> 
	<motif catenaSource="7" idSource="12 16 18" numberItems="3" > 
		<proteina name="2euh" catenaSearch="2" idSearch="25 34 37"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="2euh" catenaSearch="2" idSearch="22 37 39"/>
	</motif> 
	<motif catenaSource="9" idSource="12 14 21" numberItems="3" > 
		<proteina name="2euh" catenaSearch="2" idSearch="34 37 39"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="2euh" catenaSearch="3" idSearch="12 18 20"/>
	</motif> 
	<motif catenaSource="9" idSource="12 14 21" numberItems="3" > 
		<proteina name="2euh" catenaSearch="3" idSearch="33 36 38"/>
	</motif> 
	<motif catenaSource="6" idSource="2 5 7" numberItems="3" > 
		<proteina name="1a9x" catenaSearch="10" idSearch="1 2 4"/>
	</motif> 
	<motif catenaSource="7" idSource="7 13 15" numberItems="3" > 
		<proteina name="1a9x" catenaSearch="7" idSearch="22 27 29"/>
		<proteina name="1a9x" catenaSearch="11" idSearch="22 27 29"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="1a9x" catenaSearch="12" idSearch="44 46 49"/>
	</motif> 
	<motif catenaSource="7" idSource="0 3 14" numberItems="3" > 
		<proteina name="1a9x" catenaSearch="15" idSearch="16 27 28"/>
	</motif> 
	<motif catenaSource="7" idSource="7 13 15" numberItems="3" > 
		<proteina name="1a9x" catenaSearch="15" idSearch="22 27 29"/>
	</motif> 
	<motif catenaSource="9" idSource="7 12 14" numberItems="3" > 
		<proteina name="1a9x" catenaSearch="3" idSearch="22 26 28"/>
		<proteina name="1a9x" catenaSearch="11" idSearch="22 27 29"/>
	</motif> 
	<motif catenaSource="6" idSource="1 4 6" numberItems="3" > 
		<proteina name="2fug" catenaSearch="4" idSearch="19 23 25"/>
	</motif> 
	<motif catenaSource="6" idSource="13 16 20" numberItems="3" > 
		<proteina name="2fug" catenaSearch="4" idSearch="19 23 25"/>
	</motif> 
	<motif catenaSource="6" idSource="1 4 6" numberItems="3" > 
		<proteina name="2fug" catenaSearch="19" idSearch="18 22 24"/>
		<proteina name="2fug" catenaSearch="34" idSearch="19 22 24"/>
	</motif> 
	<motif catenaSource="6" idSource="13 16 20" numberItems="3" > 
		<proteina name="2fug" catenaSearch="49" idSearch="19 22 24"/>
	</motif> 
	<motif catenaSource="7" idSource="4 6 9" numberItems="3" > 
		<proteina name="2fug" catenaSearch="4" idSearch="19 23 25"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="2fug" catenaSearch="4" idSearch="19 23 25"/>
	</motif> 
	<motif catenaSource="7" idSource="9 14 16" numberItems="3" > 
		<proteina name="2fug" catenaSearch="14" idSearch="1 3 5"/>
		<proteina name="2fug" catenaSearch="44" idSearch="1 3 5"/>
	</motif> 
	<motif catenaSource="7" idSource="4 6 9" numberItems="3" > 
		<proteina name="2fug" catenaSearch="49" idSearch="19 22 24"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="2fug" catenaSearch="49" idSearch="19 22 24"/>
	</motif> 
	<motif catenaSource="1" idSource="6 7 10" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="16" idSearch="25 28 33"/>
	</motif> 
	<motif catenaSource="3" idSource="6 7 10" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="16" idSearch="25 28 33"/>
	</motif> 
	<motif catenaSource="5" idSource="7 8 11" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="16" idSearch="25 28 33"/>
	</motif> 
	<motif catenaSource="7" idSource="12 14 16" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="2" idSearch="16 19 20"/>
	</motif> 
	<motif catenaSource="7" idSource="13 15 17" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="2" idSearch="6 12 29"/>
	</motif> 
	<motif catenaSource="7" idSource="9 16 18" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="3" idSearch="1 15 20"/>
	</motif> 
	<motif catenaSource="7" idSource="12 14 16" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="5" idSearch="15 17 18"/>
	</motif> 
	<motif catenaSource="7" idSource="13 15 17" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="5" idSearch="6 11 28"/>
	</motif> 
	<motif catenaSource="7" idSource="12 14 16" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="6" idSearch="10 15 18"/>
		<proteina name="2hg4" catenaSearch="8" idSearch="15 18 19"/>
	</motif> 
	<motif catenaSource="7" idSource="13 15 17" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="8" idSearch="6 12 28"/>
	</motif> 
	<motif catenaSource="7" idSource="12 14 16" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="17" idSearch="16 18 19"/>
	</motif> 
	<motif catenaSource="7" idSource="13 15 17" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="17" idSearch="6 12 28"/>
	</motif> 
	<motif catenaSource="9" idSource="11 13 15" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="2" idSearch="16 19 20"/>
	</motif> 
	<motif catenaSource="9" idSource="9 15 17" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="3" idSearch="1 15 20"/>
	</motif> 
	<motif catenaSource="9" idSource="11 13 15" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="5" idSearch="15 17 18"/>
		<proteina name="2hg4" catenaSearch="6" idSearch="10 15 18"/>
		<proteina name="2hg4" catenaSearch="8" idSearch="15 18 19"/>
		<proteina name="2hg4" catenaSearch="17" idSearch="16 18 19"/>
	</motif> 
	<motif catenaSource="7" idSource="4 12 14" numberItems="3" > 
		<proteina name="2iub" catenaSearch="0" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="7" idSource="6 12 14" numberItems="3" > 
		<proteina name="2iub" catenaSearch="0" idSearch="1 4 10"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="2iub" catenaSearch="0" idSearch="0 4 10"/>
	</motif> 
	<motif catenaSource="7" idSource="6 12 14" numberItems="3" > 
		<proteina name="2iub" catenaSearch="2" idSearch="1 4 10"/>
		<proteina name="2iub" catenaSearch="4" idSearch="1 4 10"/>
		<proteina name="2iub" catenaSearch="6" idSearch="1 4 10"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="2iub" catenaSearch="6" idSearch="0 4 10"/>
	</motif> 
	<motif catenaSource="7" idSource="4 12 14" numberItems="3" > 
		<proteina name="2iub" catenaSearch="8" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="7" idSource="6 12 14" numberItems="3" > 
		<proteina name="2iub" catenaSearch="8" idSearch="1 4 10"/>
	</motif> 
	<motif catenaSource="7" idSource="4 12 14" numberItems="3" > 
		<proteina name="2iub" catenaSearch="10" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="7" idSource="6 12 14" numberItems="3" > 
		<proteina name="2iub" catenaSearch="10" idSearch="1 4 10"/>
		<proteina name="2iub" catenaSearch="12" idSearch="1 4 10"/>
	</motif> 
	<motif catenaSource="7" idSource="4 12 14" numberItems="3" > 
		<proteina name="2iub" catenaSearch="14" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="7" idSource="6 12 14" numberItems="3" > 
		<proteina name="2iub" catenaSearch="14" idSearch="1 4 10"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="2iub" catenaSearch="14" idSearch="0 4 10"/>
	</motif> 
	<motif catenaSource="7" idSource="6 12 14" numberItems="3" > 
		<proteina name="2iub" catenaSearch="16" idSearch="1 4 10"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="2iub" catenaSearch="16" idSearch="0 4 10"/>
	</motif> 
	<motif catenaSource="7" idSource="6 12 14" numberItems="3" > 
		<proteina name="2iub" catenaSearch="18" idSearch="1 4 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 6 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="0" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 11 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="0" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="0" idSearch="0 4 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 6 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="4" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 11 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="4" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 6 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="6" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 11 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="6" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="6" idSearch="0 4 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 6 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="8" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 11 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="8" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 6 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="10" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 11 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="10" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 6 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="12" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 11 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="12" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 6 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="14" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 11 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="14" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="14" idSearch="0 4 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 6 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="16" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 11 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="16" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="16" idSearch="0 4 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 6 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="18" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="9" idSource="4 11 13" numberItems="3" > 
		<proteina name="2iub" catenaSearch="18" idSearch="4 7 10"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="2otc" catenaSearch="0" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="2otc" catenaSearch="0" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="2otc" catenaSearch="1" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="2otc" catenaSearch="1" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="7" idSource="13 15 17" numberItems="3" > 
		<proteina name="2otc" catenaSearch="1" idSearch="11 13 18"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="2otc" catenaSearch="2" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="2otc" catenaSearch="2" idSearch="2 6 8"/>
		<proteina name="2otc" catenaSearch="3" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="7" idSource="13 15 17" numberItems="3" > 
		<proteina name="2otc" catenaSearch="3" idSearch="11 13 18"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="2otc" catenaSearch="4" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="2otc" catenaSearch="4" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="2otc" catenaSearch="5" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="2otc" catenaSearch="5" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="7" idSource="13 15 17" numberItems="3" > 
		<proteina name="2otc" catenaSearch="5" idSearch="11 13 18"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="2otc" catenaSearch="6" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="2otc" catenaSearch="6" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="7" idSource="13 15 17" numberItems="3" > 
		<proteina name="2otc" catenaSearch="6" idSearch="11 13 18"/>
	</motif> 
	<motif catenaSource="7" idSource="6 9 12" numberItems="3" > 
		<proteina name="2otc" catenaSearch="7" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="2otc" catenaSearch="7" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="7" idSource="13 15 17" numberItems="3" > 
		<proteina name="2otc" catenaSearch="7" idSearch="11 13 18"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="2otc" catenaSearch="8" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="7" idSource="13 15 17" numberItems="3" > 
		<proteina name="2otc" catenaSearch="8" idSearch="11 13 18"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 13" numberItems="3" > 
		<proteina name="2otc" catenaSearch="0" idSearch="2 6 8"/>
		<proteina name="2otc" catenaSearch="1" idSearch="2 6 8"/>
		<proteina name="2otc" catenaSearch="2" idSearch="2 6 8"/>
		<proteina name="2otc" catenaSearch="3" idSearch="2 6 8"/>
		<proteina name="2otc" catenaSearch="4" idSearch="2 6 8"/>
		<proteina name="2otc" catenaSearch="5" idSearch="2 6 8"/>
		<proteina name="2otc" catenaSearch="6" idSearch="2 6 8"/>
		<proteina name="2otc" catenaSearch="7" idSearch="2 6 8"/>
		<proteina name="2otc" catenaSearch="8" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="2o8r" catenaSearch="3" idSearch="8 10 12"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 13" numberItems="3" > 
		<proteina name="2o8r" catenaSearch="3" idSearch="8 10 12"/>
	</motif> 
	<motif catenaSource="9" idSource="1 9 11" numberItems="3" > 
		<proteina name="2o8r" catenaSearch="3" idSearch="10 12 16"/>
	</motif> 
	<motif catenaSource="6" idSource="10 13 20" numberItems="3" > 
		<proteina name="2o8r" catenaSearch="7" idSearch="8 11 12"/>
	</motif> 
	<motif catenaSource="7" idSource="6 12 14" numberItems="3" > 
		<proteina name="2o8r" catenaSearch="7" idSearch="19 21 26"/>
	</motif> 
	<motif catenaSource="7" idSource="6 14 16" numberItems="3" > 
		<proteina name="2pva" catenaSearch="0" idSearch="0 27 28"/>
		<proteina name="2pva" catenaSearch="1" idSearch="0 27 28"/>
		<proteina name="2pva" catenaSearch="3" idSearch="0 26 27"/>
	</motif> 
	<motif catenaSource="7" idSource="14 16 18" numberItems="3" > 
		<proteina name="2r6g" catenaSearch="0" idSearch="27 28 29"/>
	</motif> 
	<motif catenaSource="7" idSource="12 14 16" numberItems="3" > 
		<proteina name="3cu7" catenaSearch="0" idSearch="4 8 9"/>
	</motif> 
	<motif catenaSource="9" idSource="11 13 15" numberItems="3" > 
		<proteina name="3cu7" catenaSearch="0" idSearch="4 8 9"/>
	</motif> 
	<motif catenaSource="7" idSource="12 14 16" numberItems="3" > 
		<proteina name="3cu7" catenaSearch="4" idSearch="1 7 8"/>
	</motif> 
	<motif catenaSource="7" idSource="9 14 16" numberItems="3" > 
		<proteina name="3cu7" catenaSearch="4" idSearch="5 7 8"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 13" numberItems="3" > 
		<proteina name="3cu7" catenaSearch="4" idSearch="1 5 7"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 15" numberItems="3" > 
		<proteina name="3cu7" catenaSearch="4" idSearch="1 5 8"/>
	</motif> 
	<motif catenaSource="9" idSource="11 13 15" numberItems="3" > 
		<proteina name="3cu7" catenaSearch="4" idSearch="1 7 8"/>
	</motif> 
	<motif catenaSource="9" idSource="9 13 15" numberItems="3" > 
		<proteina name="3cu7" catenaSearch="4" idSearch="5 7 8"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 13 15" numberItems="4" > 
		<proteina name="3cu7" catenaSearch="4" idSearch="1 5 7 8"/>
	</motif> 
	<motif catenaSource="7" idSource="12 14 16" numberItems="3" > 
		<proteina name="3cu7" catenaSearch="9" idSearch="1 7 8"/>
	</motif> 
	<motif catenaSource="7" idSource="9 14 16" numberItems="3" > 
		<proteina name="3cu7" catenaSearch="9" idSearch="5 7 8"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 13" numberItems="3" > 
		<proteina name="3cu7" catenaSearch="9" idSearch="1 5 7"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 15" numberItems="3" > 
		<proteina name="3cu7" catenaSearch="9" idSearch="1 5 8"/>
	</motif> 
	<motif catenaSource="9" idSource="11 13 15" numberItems="3" > 
		<proteina name="3cu7" catenaSearch="9" idSearch="1 7 8"/>
	</motif> 
	<motif catenaSource="9" idSource="9 13 15" numberItems="3" > 
		<proteina name="3cu7" catenaSearch="9" idSearch="5 7 8"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 13 15" numberItems="4" > 
		<proteina name="3cu7" catenaSearch="9" idSearch="1 5 7 8"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="3kg2" catenaSearch="6" idSearch="9 14 16"/>
		<proteina name="3mx5" catenaSearch="2" idSearch="1 2 3"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 13" numberItems="3" > 
		<proteina name="3mx5" catenaSearch="2" idSearch="1 2 3"/>
	</motif> 
	<motif catenaSource="9" idSource="4 6 9" numberItems="3" > 
		<proteina name="3mx5" catenaSearch="2" idSearch="1 2 3"/>
	</motif> 
	<motif catenaSource="9" idSource="6 9 11" numberItems="3" > 
		<proteina name="3mx5" catenaSearch="2" idSearch="1 2 3"/>
	</motif> 
	<motif catenaSource="7" idSource="9 12 14" numberItems="3" > 
		<proteina name="3mx5" catenaSearch="10" idSearch="1 2 3"/>
	</motif> 
	<motif catenaSource="9" idSource="9 11 13" numberItems="3" > 
		<proteina name="3mx5" catenaSearch="10" idSearch="1 2 3"/>
	</motif> 
	<motif catenaSource="9" idSource="6 9 11" numberItems="3" > 
		<proteina name="3mx5" catenaSearch="10" idSearch="1 2 3"/>
	</motif> 
	<motif catenaSource="9" idSource="4 6 9" numberItems="3" > 
		<proteina name="3mx5" catenaSearch="10" idSearch="1 2 3"/>
	</motif> 
</test> 

