<test protein="2hzb">
	<motif catenaSource="2" idSource="2 8 15" numberItems="3" > 
		<proteina name="1e8c" catenaSearch="0" idSearch="11 13 18"/>
	</motif> 
	<motif catenaSource="0" idSource="0 14 20" numberItems="3" > 
		<proteina name="1a9x" catenaSearch="8" idSearch="43 44 46"/>
	</motif> 
	<motif catenaSource="3" idSource="0 15 21" numberItems="3" > 
		<proteina name="1a9x" catenaSearch="8" idSearch="43 44 46"/>
	</motif> 
	<motif catenaSource="0" idSource="0 2 14" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="0" idSearch="9 10 11"/>
	</motif> 
	<motif catenaSource="1" idSource="0 2 19" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="0" idSearch="30 33 62"/>
	</motif> 
	<motif catenaSource="2" idSource="0 2 15" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="0" idSearch="43 44 45"/>
	</motif> 
	<motif catenaSource="2" idSource="0 2 19" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="0" idSearch="30 33 62"/>
		<proteina name="2b5l" catenaSearch="3" idSearch="1 3 4"/>
	</motif> 
	<motif catenaSource="3" idSource="0 15 19" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="0" idSearch="56 57 58"/>
		<proteina name="2b5l" catenaSearch="0" idSearch="55 56 57"/>
		<proteina name="2b5l" catenaSearch="0" idSearch="10 11 12"/>
	</motif> 
	<motif catenaSource="3" idSource="0 2 15" numberItems="3" > 
		<proteina name="2b5l" catenaSearch="0" idSearch="9 10 11"/>
	</motif> 
	<motif catenaSource="0" idSource="0 2 14" numberItems="3" > 
		<proteina name="2bcc" catenaSearch="0" idSearch="1 2 16"/>
	</motif> 
	<motif catenaSource="1" idSource="0 2 8" numberItems="3" > 
		<proteina name="2bcc" catenaSearch="0" idSearch="1 2 16"/>
	</motif> 
	<motif catenaSource="1" idSource="0 8 19" numberItems="3" > 
		<proteina name="2bcc" catenaSearch="0" idSearch="1 8 16"/>
	</motif> 
	<motif catenaSource="2" idSource="0 8 19" numberItems="3" > 
		<proteina name="2bcc" catenaSearch="0" idSearch="1 8 16"/>
	</motif> 
	<motif catenaSource="2" idSource="0 2 19" numberItems="3" > 
		<proteina name="2bcc" catenaSearch="1" idSearch="5 6 14"/>
	</motif> 
	<motif catenaSource="3" idSource="0 2 15" numberItems="3" > 
		<proteina name="2bcc" catenaSearch="0" idSearch="1 2 16"/>
	</motif> 
	<motif catenaSource="1" idSource="0 2 15" numberItems="3" > 
		<proteina name="2ex3" catenaSearch="0" idSearch="0 1 2"/>
		<proteina name="2fug" catenaSearch="29" idSearch="3 4 5"/>
		<proteina name="2fug" catenaSearch="44" idSearch="3 4 5"/>
	</motif> 
	<motif catenaSource="3" idSource="0 2 15" numberItems="3" > 
		<proteina name="2fug" catenaSearch="29" idSearch="3 4 5"/>
		<proteina name="2fug" catenaSearch="44" idSearch="3 4 5"/>
	</motif> 
	<motif catenaSource="0" idSource="0 14 18" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="5" idSearch="6 28 30"/>
	</motif> 
	<motif catenaSource="1" idSource="0 2 19" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="0" idSearch="15 18 20"/>
		<proteina name="2hg4" catenaSearch="3" idSearch="15 18 20"/>
		<proteina name="2hg4" catenaSearch="6" idSearch="15 18 20"/>
	</motif> 
	<motif catenaSource="1" idSource="0 2 8" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="8" idSearch="0 3 5"/>
	</motif> 
	<motif catenaSource="1" idSource="0 2 19" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="9" idSearch="13 16 18"/>
		<proteina name="2hg4" catenaSearch="13" idSearch="14 17 19"/>
		<proteina name="2hg4" catenaSearch="16" idSearch="15 18 20"/>
	</motif> 
	<motif catenaSource="1" idSource="0 2 8" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="17" idSearch="0 3 5"/>
	</motif> 
	<motif catenaSource="2" idSource="0 2 19" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="0" idSearch="15 18 20"/>
		<proteina name="2hg4" catenaSearch="3" idSearch="15 18 20"/>
	</motif> 
	<motif catenaSource="2" idSource="0 2 8" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="8" idSearch="0 3 5"/>
	</motif> 
	<motif catenaSource="2" idSource="0 2 19" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="9" idSearch="13 16 18"/>
	</motif> 
	<motif catenaSource="2" idSource="0 2 8" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="11" idSearch="0 3 5"/>
	</motif> 
	<motif catenaSource="2" idSource="0 2 19" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="13" idSearch="14 17 19"/>
		<proteina name="2hg4" catenaSearch="16" idSearch="15 18 20"/>
	</motif> 
	<motif catenaSource="2" idSource="0 2 8" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="17" idSearch="0 3 5"/>
	</motif> 
	<motif catenaSource="3" idSource="0 15 19" numberItems="3" > 
		<proteina name="2hg4" catenaSearch="5" idSearch="6 28 30"/>
	</motif> 
	<motif catenaSource="0" idSource="0 14 18" numberItems="3" > 
		<proteina name="2otc" catenaSearch="0" idSearch="11 18 22"/>
		<proteina name="2otc" catenaSearch="2" idSearch="11 18 22"/>
	</motif> 
	<motif catenaSource="0" idSource="0 2 14" numberItems="3" > 
		<proteina name="2otc" catenaSearch="4" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="0" idSource="0 14 18" numberItems="3" > 
		<proteina name="2otc" catenaSearch="5" idSearch="11 18 22"/>
		<proteina name="2otc" catenaSearch="7" idSearch="11 18 22"/>
	</motif> 
	<motif catenaSource="3" idSource="0 15 19" numberItems="3" > 
		<proteina name="2otc" catenaSearch="0" idSearch="11 18 22"/>
		<proteina name="2otc" catenaSearch="1" idSearch="11 18 22"/>
		<proteina name="2otc" catenaSearch="2" idSearch="11 18 22"/>
		<proteina name="2otc" catenaSearch="3" idSearch="11 18 22"/>
		<proteina name="2otc" catenaSearch="4" idSearch="11 18 22"/>
	</motif> 
	<motif catenaSource="3" idSource="0 2 15" numberItems="3" > 
		<proteina name="2otc" catenaSearch="4" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="3" idSource="0 15 19" numberItems="3" > 
		<proteina name="2otc" catenaSearch="5" idSearch="11 18 22"/>
	</motif> 
	<motif catenaSource="3" idSource="0 2 15" numberItems="3" > 
		<proteina name="2otc" catenaSearch="5" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="3" idSource="0 15 19" numberItems="3" > 
		<proteina name="2otc" catenaSearch="6" idSearch="11 18 22"/>
	</motif> 
	<motif catenaSource="3" idSource="0 2 15" numberItems="3" > 
		<proteina name="2otc" catenaSearch="6" idSearch="2 6 8"/>
	</motif> 
	<motif catenaSource="3" idSource="0 15 19" numberItems="3" > 
		<proteina name="2otc" catenaSearch="7" idSearch="11 18 22"/>
	</motif> 
	<motif catenaSource="2" idSource="0 2 19" numberItems="3" > 
		<proteina name="3kg2" catenaSearch="3" idSearch="9 12 16"/>
	</motif> 
	<motif catenaSource="0" idSource="14 18 23" numberItems="3" > 
		<proteina name="3mx5" catenaSearch="2" idSearch="2 8 10"/>
		<proteina name="3mx5" catenaSearch="10" idSearch="2 8 10"/>
	</motif> 
	<motif catenaSource="0" idSource="14 20 23" numberItems="3" > 
		<proteina name="3mx5" catenaSearch="10" idSearch="2 8 10"/>
	</motif> 
	<motif catenaSource="1" idSource="15 19 24" numberItems="3" > 
		<proteina name="3mx5" catenaSearch="2" idSearch="2 8 10"/>
		<proteina name="3mx5" catenaSearch="10" idSearch="2 8 10"/>
	</motif> 
	<motif catenaSource="2" idSource="15 19 24" numberItems="3" > 
		<proteina name="3mx5" catenaSearch="2" idSearch="2 8 10"/>
	</motif> 
	<motif catenaSource="2" idSource="15 21 24" numberItems="3" > 
		<proteina name="3mx5" catenaSearch="10" idSearch="2 8 10"/>
	</motif> 
	<motif catenaSource="3" idSource="15 19 24" numberItems="3" > 
		<proteina name="3mx5" catenaSearch="2" idSearch="2 8 10"/>
	</motif> 
	<motif catenaSource="3" idSource="15 21 24" numberItems="3" > 
		<proteina name="3mx5" catenaSearch="10" idSearch="2 8 10"/>
	</motif> 
	<motif catenaSource="0" idSource="8 14 18" numberItems="3" > 
		<proteina name="2o8r" catenaSearch="7" idSearch="22 24 26"/>
	</motif> 
	<motif catenaSource="0" idSource="0 2 14" numberItems="3" > 
		<proteina name="2o8r" catenaSearch="7" idSearch="19 21 22"/>
	</motif> 
	<motif catenaSource="1" idSource="0 8 19" numberItems="3" > 
		<proteina name="2o8r" catenaSearch="7" idSearch="19 22 26"/>
	</motif> 
	<motif catenaSource="3" idSource="0 2 15" numberItems="3" > 
		<proteina name="2o8r" catenaSearch="7" idSearch="19 21 22"/>
	</motif> 
	<motif catenaSource="3" idSource="8 15 19" numberItems="3" > 
		<proteina name="2o8r" catenaSearch="7" idSearch="22 24 26"/>
	</motif> 
	<motif catenaSource="1" idSource="0 2 21" numberItems="3" > 
		<proteina name="2pva" catenaSearch="0" idSearch="2 8 14"/>
	</motif> 
	<motif catenaSource="1" idSource="0 2 19" numberItems="3" > 
		<proteina name="2pva" catenaSearch="0" idSearch="8 15 16"/>
	</motif> 
	<motif catenaSource="2" idSource="0 2 21" numberItems="3" > 
		<proteina name="2pva" catenaSearch="0" idSearch="2 8 14"/>
	</motif> 
	<motif catenaSource="0" idSource="18 20 23" numberItems="3" > 
		<proteina name="3pga" catenaSearch="1" idSearch="11 14 15"/>
	</motif> 
	<motif catenaSource="1" idSource="15 19 24" numberItems="3" > 
		<proteina name="3pga" catenaSearch="1" idSearch="5 7 11"/>
	</motif> 
	<motif catenaSource="1" idSource="19 21 24" numberItems="3" > 
		<proteina name="3pga" catenaSearch="1" idSearch="11 14 15"/>
	</motif> 
	<motif catenaSource="2" idSource="15 19 24" numberItems="3" > 
		<proteina name="3pga" catenaSearch="1" idSearch="5 7 11"/>
	</motif> 
	<motif catenaSource="2" idSource="19 21 24" numberItems="3" > 
		<proteina name="3pga" catenaSearch="1" idSearch="11 14 15"/>
	</motif> 
	<motif catenaSource="3" idSource="15 19 24" numberItems="3" > 
		<proteina name="3pga" catenaSearch="1" idSearch="5 7 11"/>
	</motif> 
	<motif catenaSource="3" idSource="19 21 24" numberItems="3" > 
		<proteina name="3pga" catenaSearch="1" idSearch="11 14 15"/>
	</motif> 
	<motif catenaSource="3" idSource="15 19 24" numberItems="3" > 
		<proteina name="3pga" catenaSearch="3" idSearch="5 7 11"/>
	</motif> 
	<motif catenaSource="2" idSource="15 19 24" numberItems="3" > 
		<proteina name="3pga" catenaSearch="5" idSearch="5 7 11"/>
	</motif> 
	<motif catenaSource="0" idSource="14 18 23" numberItems="3" > 
		<proteina name="3pga" catenaSearch="7" idSearch="5 7 11"/>
	</motif> 
	<motif catenaSource="2" idSource="15 19 24" numberItems="3" > 
		<proteina name="3pga" catenaSearch="7" idSearch="5 7 11"/>
	</motif> 
	<motif catenaSource="3" idSource="15 19 24" numberItems="3" > 
		<proteina name="3pga" catenaSearch="7" idSearch="5 7 11"/>
	</motif> 
</test> 

