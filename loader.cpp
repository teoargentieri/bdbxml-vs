#include "loader.h"
#include <QDir>

using namespace DbXml;
Loader::Loader()
{
    caricamento=false;
    //we open manager and container
    XmlContainerConfig config=XmlContainerConfig();
    //if container doesn't exist than create it
    config.setAllowCreate(true);
    manager.setDefaultContainerConfig(config);
    container=manager.openContainer("ContainerComparison.dbxml");
    context = manager.createUpdateContext();

}

Loader::Loader(const char *containerName)
{
    caricamento=false;
    //we open manager and container
    XmlContainerConfig config=XmlContainerConfig();
    //if container doesn't exist than create it
    config.setAllowCreate(true);
    manager.setDefaultContainerConfig(config);
    container=manager.openContainer(containerName);
    context = manager.createUpdateContext();
}

void Loader::setContainer(std::string containerName){
    try{
        container.close();
        XmlContainerConfig config=XmlContainerConfig();
        //if container doesn't exist than create it
        config.setAllowCreate(true);
        manager.setDefaultContainerConfig(config);
        container=manager.openContainer(containerName);
    }catch(XmlException xe){
        std::cout<<xe.what()<<std::endl;
    }
}

//carico nel container il documento indicato in filePath con nome nomeDoc
void Loader::loadDataFromFile(const char *filePath, const char *nomeDoc){
    XmlInputStream* file = manager.createLocalFileInputStream(filePath);

        try
        {
            std::string s = container.putDocument(nomeDoc, file, context, 0);
            std::cout << "Caricamento del documento:\n";
            std::cout << s+"/n";
            caricamento=true;
        }
        catch (XmlException xmlEr)
        {
            std::cout << "errore\n";
            std::cout<<xmlEr.what()<<std::endl;
        }
}
void Loader::loadDataFromFile(const char *filePath){
    std::string nomeDoc=filePath;
        nomeDoc=nomeDoc.substr(nomeDoc.find_last_of("/")+1);
        XmlInputStream* file = manager.createLocalFileInputStream(filePath);
        try
        {
            std::string s = container.putDocument(nomeDoc, file, context, 0);
            std::cout << "Caricamento del documento:\n";
            std::cout << s+"/n";
            caricamento=true;
        }
        catch (XmlException xmlEr)
        {
            std::cout << "errore\n";
            std::cout<<xmlEr.what()<<std::endl;
        }
}

void Loader::loadDataFromDirectory(const char *directoryPath){
    std::string advice="Documenti caricati: \n";
    QDir dir (directoryPath);

    QStringList filters;
        filters << "*.xml";
        dir.setNameFilters(filters);
    QFileInfoList list = dir.entryInfoList(QDir::AllEntries);
    std::string nome;
    for (int i = 1; i < list.size(); i++)
    {
        nome=list.at(i).baseName().toStdString();
        Loader::loadDataFromFile(list.at(i).filePath().toStdString().c_str(),nome.c_str());
        advice+=nome+"\n";
    }

}/*
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (directoryPath)) != NULL) {
        // print all the files and directories within directory
      while ((ent = readdir (dir)) != NULL) {
         std::string filePath=directoryPath;
         std::string nome=ent->d_name;
         filePath+="/"+nome;
         std::string format=nome.substr(nome.find_last_of(".")+1);
         if(format=="xml"){
         Loader::loadDataFromFile(filePath.c_str(),nome.c_str());
         advice+=nome+"\n";
         std::cout<<format+"\n";
      }
      }
      closedir (dir);
      if(caricamento){
          std::cout<<advice+"\n";
      }
    } else {
      // could not open directory
      perror ("");
    }


}*/


void Loader::loadFromString(std::string docname,std::string content){
    container.putDocument(docname,content,context,0);
}

void Loader::loadFromDoc(XmlDocument doc){
    container.putDocument(doc,context,0);
}
