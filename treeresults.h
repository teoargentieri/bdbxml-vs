#ifndef TREERESULTS_H
#define TREERESULTS_H

#include <QMainWindow>
#include "treemodel.h"
#include <QTreeView>
#include<QDir>
#include<iostream>
#include<fstream>
#include<QFileDialog>
#include<QFile>
#include<QMessageBox>

namespace Ui {
class TreeResults;
}

class TreeResults : public QMainWindow
{
    Q_OBJECT

public:
    explicit TreeResults(QWidget *parent = 0);
    ~TreeResults();
    void setModelView(TreeModel *model);
private slots:
    void on_actionSave_triggered();

    void on_actionSave_as_triggered();

private:
    Ui::TreeResults *ui;
    QTreeView* treeView;
    TreeModel* treeModel;
    QString dir;
    void checkDir(){
        if(!QDir(dir).exists())
           QDir().mkdir(dir);
    }
};


#endif // TREERESULTS_H
