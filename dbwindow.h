#ifndef DBWINDOW_H
#define DBWINDOW_H

#include <QMainWindow>
#include <QFrame>
#include <QLabel>
#include <QListWidget>
#include <QScrollArea>
#include <QTextBrowser>
#include "loader.h"
#include "querymanager.h"
#include "createcontainer.h"
#include <QTreeView>
#include"treemodel.h"
#include "loader.h"
#include "querymanager.h"
#include "dbxml/DbXml.hpp"
#include <QFileDialog>
#include <QSettings>
#include <iostream>
#include"treeresults.h"
#include<QComboBox>
#include <QList>

namespace Ui {
class DbWindow;
}
using namespace std;
class DbWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit DbWindow(QWidget *parent = 0);
    void newTreeView(std::string currentSource);

    ~DbWindow();


private slots:
    void on_actionChoose_directory_triggered();

    void on_actionChoose_document_triggered();

    void on_DbWindow_iconSizeChanged(const QSize &iconSize);
    void mostranomi();

    void on_actionSet_Container_triggered();

    void on_listWidget_doubleClicked(const QModelIndex &index);

    void on_actionCreate_Container_triggered();
    void createContainerAction();

    void on_listWidget_clicked(const QModelIndex &index);

    void on_startButton_clicked();
    void on_comboBox_activated(const QString &arg1);

    void on_actionSource_proteins_triggered();

    void on_actionSerach_all_triggered();

    void on_actionClear_previous_research_triggered();

private:
    Ui::DbWindow *ui;
    QFrame* central;
    QLabel* label;
    QLabel* psLabel;
    QLabel* psLabelDescription;
    QListWidget* listWidget;
    Loader loader;
    QueryManager queryManager;
    QTextBrowser* viewArea;
    CreateContainer* cr;
    QPushButton* startButton;
    QComboBox* combobox;
    std::string currentViewContainer;
    int cathegoryValue;

};

#endif // DBWINDOW_H
