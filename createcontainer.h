#ifndef CREATECONTAINER_H
#define CREATECONTAINER_H

#include <QDialog>
#include <QDialogButtonBox>

namespace Ui {
class CreateContainer;
}

class CreateContainer : public QDialog
{
    Q_OBJECT
signals:
   void mySignal();
public:
    explicit CreateContainer(QWidget *parent = 0);
    ~CreateContainer();
    bool active;
    std::string nuovoContainer;
    QDialogButtonBox* bb;

public slots:
    void on_buttonBox_accepted();

    void on_pushButton_clicked();

private:
    Ui::CreateContainer *ui;

};

#endif // CREATECONTAINER_H
