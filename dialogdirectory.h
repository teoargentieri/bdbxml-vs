#ifndef DIALOGDIRECTORY_H
#define DIALOGDIRECTORY_H

#include <QDialog>

namespace Ui {
class DialogDirectory;
}

class DialogDirectory : public QDialog
{
    Q_OBJECT

public:
    explicit DialogDirectory(QWidget *parent = 0);
    ~DialogDirectory();

private:
    Ui::DialogDirectory *ui;
};

#endif // DIALOGDIRECTORY_H
