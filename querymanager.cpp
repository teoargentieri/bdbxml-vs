
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <QList>
#include "querymanager.h"
#include "loader.h"

using namespace DbXml;


QueryManager::QueryManager()
{
    //we open manager and container
    XmlContainerConfig config=XmlContainerConfig();
    //if container doesn't exist than create it
    config.setAllowCreate(true);
    manager.setDefaultContainerConfig(config);
    container=manager.openContainer("crossMotif.dbxml");
}


QueryManager::QueryManager(const char* containerName)
{
    //we open manager and container
    XmlContainerConfig config=XmlContainerConfig();
    //if container doesn't exist than create it
    config.setAllowCreate(true);
    manager.setDefaultContainerConfig(config);
    container=manager.openContainer(containerName);
}

std::string QueryManager::doGetDocument(const std::string docname)
{
    std::string resultDocument="";
    try {
        std::cout << "Getting document '" << docname << "' from the container." << std::endl;
        //std::cout << "Return to continue: ";
        //getc(stdin);
        std::cout << "\n";

        //Get the document from the container using the document name
        XmlDocument theDocument = container.getDocument(docname,0);
        theDocument.getContent(resultDocument);

    }
    //Catches XmlException
    catch (std::exception &e) {
        std::cerr << "Get document from container failed.\n";
        std::cerr << e.what() << "\n";
        exit(-1);
    }
    return resultDocument;
}

std ::string QueryManager::doQuery(const std::string &fullQuery)
{
    // Perform the query. Result type is by default Result Document
    std::string resultDocument="";
    try {
        std::cout << "Exercising query '" << fullQuery << "' " << std::endl;
        std::cout << "Return to continue: ";

        XmlQueryContext context = manager.createQueryContext();

        XmlResults results(manager.query(fullQuery, context));
        XmlValue value;
        while (results.next(value)) {
            // Obtain the value as a string and print it to stdout
            std::cout << value.asString() << std::endl;
            resultDocument+=value.asString();
        }

        std::cout << (unsigned int)results.size()
                  << " objects returned for expression '"
                  << fullQuery << "'\n" << std::endl;

    }
    catch (XmlException &e) {
        std::cerr << "Query " << fullQuery << " failed\n";
        std::cerr << e.what() << "\n";

        exit(-1);
    }
    catch (std::exception &e) {
        std::cerr << "Query " << fullQuery << " failed\n";
        std::cerr << e.what() << "\n";

        exit(-1);
    }
    return resultDocument;
}

XmlResults QueryManager::doFlowrQuery(const std::string &query){
    //std::string query="for $a in collection('" + container.getName() + "') return dbxml:metadata(\"dbxml:name\", $a)";
    try {
        // std::cout << "Exercising query '" << query << "' " << std::endl;

        XmlQueryContext context = manager.createQueryContext();

        XmlResults results(manager.query(query, context));
        return results;
    }catch(XmlException xe){
        std::cout << xe.what() << std::endl;

    }
}

XmlResults QueryManager::doFlowrQuery(const std::string &query,XmlQueryContext context){
    try {
        std::cout << "Exercising query '" << query << "' " << std::endl;

        XmlResults results(manager.query(query, context));
        return results;
    }catch(XmlException xe){
        std::cout << xe.what() << std::endl;

    }
}

XmlResults QueryManager::ricercaCiclo(std::string nomeProteina){
    std::vector<std::string> avoided;
    avoided.reserve(1);
    avoided.push_back(nomeProteina);
    std::string iterativeQuery,resultDocument="",attachedQuery="";
    try {
        std::cout << "Exercising query in container "+container.getName() << std::endl;

        XmlQueryContext context = manager.createQueryContext();
        std::string query="for $a in collection('" + container.getName() + "') where $a/root/source-protein/@name="+"\""+nomeProteina+"\""+" return (data($a/root/source-protein/@name),data($a/root/search-protein/@name))";
        XmlResults results(manager.query(query, context));
        XmlValue value;
        for (int var = 0; var < results.size()/2; ++var) {
            results.next(value);
            results.next(value);
            std::cout<<"prima ricerca "+value.asString()<<std::endl;
            avoided.push_back(value.asString());
            iterativeQuery="for $a in collection('" + container.getName() + "') where $a/root/source-protein/@name="+"\""+value.asString()+"\"";
            for (int j = 0; j < avoided.size(); j++) {
                attachedQuery+=" and $a/root/search-protein/@name!=\""+avoided[j]+"\"";
            }
            iterativeQuery+=attachedQuery+"return $a";
            std::cout<<iterativeQuery<<std::endl;
            XmlResults iterativeResults(manager.query(iterativeQuery, context));
            XmlValue iterativeValue;
            while(iterativeResults.next(iterativeValue)){
                std::cout<<iterativeResults.size()+"\n";
                resultDocument+=iterativeValue.asString()+"\n";
            }
            attachedQuery="";
            iterativeQuery="";
            std::cout<<"dimensione del vettore: ";
            std::printf("%d /n",avoided.size());
        }
        std::cout<<resultDocument<<std::endl;
        return results;
    }catch(XmlException xe){
        std::cout << xe.what() << std::endl;

    }
}


inline bool QueryManager::exists_test (const std::string& name) {
    struct stat buffer;
    return (stat (name.c_str(), &buffer) == 0);
}
void QueryManager::setContainer(std::string containerName){
    try{
        //container.close();
        XmlContainerConfig config=XmlContainerConfig();
        //if container doesn't exist than create it
        config.setAllowCreate(true);
        manager.setDefaultContainerConfig(config);
        container=manager.openContainer(containerName);
    }catch(XmlException xe){
        std::cout<<xe.what()<<std::endl;
    }
}

/*Bisogna stare attenti ai containers utilizzati come default, molto probabilmente si dovrà togliere proprio la possibilità
di scegliere il container adatto oppure creare un file di configurazione in cui l'utente sceglie i container e poi questi
vengono letti in modo dinamico dall'utente*/
void QueryManager::crossSearch(std::string nomeProteina){


    QList<candidato*> parziale;
    parziale.reserve(1);
    candidato* candidatoPar=new candidato;

    container=manager.openContainer(startingContainer);
    if(exists_test("containerAnalisi.dbxml"))
        remove("containerAnalisi.dbxml");
    if(exists_test("risultati.dbxml"))
        remove("risultati.dbxml");

    std::cout<<"Loader impostato in containerAnalisi"<<std::endl;
    Loader ld=Loader();
	ld.setContainer("containerAnalisi.dbxml");
    //c'era impostato un container di default diverso, ora metto quello che sceglie l'utente (anche se si parte sempre da ContainerComparison)
    //this->setContainer("crossMotif.dbxml");

    XmlQueryContext context=manager.createQueryContext();
    context.setEvaluationType(XmlQueryContext::Lazy);
    std::string query;
    std::cout<<"Start evaluation"<<std::endl;

    //query per recuperare tutti i matches candidati per essere motif partendo dalla proteina scelta

    query=" declare namespace functx=\"http://www.functx.com\"; \n";
    query+=" declare function functx:mysort ( $seq as item()* ) as item()* {for $item in $seq order by number($item) return $item} ; \n ";
    query+=" for $pMotif in collection(\""+this->container.getName()+"\")/search-result/search-result-chains/search-result-chain/search-hits/search-hit  \n";
    query+=" let $p:= $pMotif/../../../..  \n";
    query+=" where $p/source-protein/@name=\""+nomeProteina+"\" or $p/search-protein/@name=\""+nomeProteina+"\" \n";
    //query+=" and count(distinct-values(data($pMotif/source-motif/ss-index/@id)))<=6 \n" ;
    //query+=" order by $pMotif/@source-chain , string-join(distinct-values(data($pMotif/source-motif/ss-index/@id)),' '),$p/search-protein/@name \n";
    query+=" return \n"; //, string-join(distinct-values(data($pMotif/source-motif/ss-index/@id)),' ')"; //"data($p/search-protein/@name),data($pMotif/@source-chain),data($pMotif/@search-chain),string-join(distinct-values(data($pMotif/source-motif/ss-index/@id)),' '),count(distinct-values(data($pMotif/source-motif/ss-index/@id))),string-join(functx:mysort(distinct-values(data(($pMotif/peak/vote/@ss1-id,$pMotif/peak/vote/@ss2-id)))),' ')) \n";
    query+=" if($p/source-protein/@name=\""+nomeProteina+"\" ) \n";
    query+=" then (count(distinct-values(data($pMotif/source-motif/ss-index/@id))),data($p/search-protein/@name),data($pMotif/@source-chain),data($pMotif/@search-chain),1,string-join(distinct-values(data($pMotif/source-motif/ss-index/@id)),' '),string-join(functx:mysort(distinct-values(data(($pMotif/peak/vote/@ss1-id,$pMotif/peak/vote/@ss2-id)))),' '))\n";
    query+=" else (count(distinct-values(data($pMotif/source-motif/ss-index/@id))),data($p/source-protein/@name),data($pMotif/@search-chain),data($pMotif/@source-chain),0,string-join(distinct-values(data($pMotif/source-motif/ss-index/@id)),' '),string-join(functx:mysort(distinct-values(data(($pMotif/peak/vote/@ss1-id,$pMotif/peak/vote/@ss2-id)))),' ')) \n";


    XmlResults r=doFlowrQuery(query);
    XmlValue v;
    std::string catenaSource,catenaSearch,nSS,idSource,idSearch,searchProtein,xml="";
    std::string lastId="a",lastC="a";

    //creazione documento temporaneo per il salvataggio dei risultati parziali

    xml="<test protein=\""+nomeProteina+"\">\n";
    for (int var = 0; var < r.size()/7; ++var) {
        r.next(v);
        nSS=v.asString();
        r.next(v);
        searchProtein=v.asString();
        r.next(v);
        catenaSource=v.asString();
        r.next(v);
        catenaSearch=v.asString();
        r.next(v);
        if(v.asNumber()==1){
            r.next(v);
            idSource=v.asString();
            r.next(v);
            idSearch=v.asString();
        }else{
            r.next(v);
            idSearch=v.asString();
            r.next(v);
            idSource=v.asString();
        }

        if(catenaSource!=lastC | idSource!=lastId){
            if(var>0){
                xml+="\t</motif> \n";
            }
            xml+="\t<motif catenaSource=\""+catenaSource+"\" idSource=\""+idSource+"\" numberItems=\""+nSS+"\" > \n";
            lastC=catenaSource;
            lastId=idSource;
        }
        xml+="\t\t<proteina name=\""+searchProtein+"\" catenaSearch=\""+catenaSearch+"\" idSearch=\""+idSearch+"\"/>\n";

    }

    xml+="\t</motif> \n";
    xml+="</test> \n";

    lastId="a",lastC="a";

    //std::cout<<xml<<std::endl;
	cout<<"Loader impostato in "<<ld.container.getName()<<endl;
    ld.loadFromString(nomeProteina+"_analisi",xml);
    ld.container.close();
    std::string fileName="test"+nomeProteina+".txt";

    std::ofstream outfile(fileName.c_str());
    outfile << xml << std::endl;
    outfile.close();

    setContainer("containerAnalisi.dbxml");

    query="for $hit in collection('" + container.getName() + "')/test/motif/proteina ";
    query+=" let $hitM:=$hit/.. ";
    query+=" where data($hit/@name)!=\""+nomeProteina+"\" ";
    query+=" order by data($hit/../@catenaSource), data($hit/../@idSource),data($hit/@name),data($hit/@catenaSearch), data($hit/@idSearch) ";
    query+="return (data($hit/@name),data($hit/@catenaSearch) ,data($hitM/@idSource),data($hitM/@catenaSource),data($hit/@idSearch),data($hitM/@numberItems))";

    //cout<<"QUERY"<<endl<<query<<endl;
    XmlContainer container=manager.openContainer(startingContainer);

    XmlResults r2;
    XmlValue v2;

    r=doFlowrQuery(query);
    std::string query2,nome,catena,id,idMotif,catenaMotif,numberItems;
    std::vector<std::string> avoided;
    avoided.reserve(1);

    std::string ax="<final protein=\""+nomeProteina+"\">\n";
    for (int var = 0; var < r.size()/6; ++var) {
        r.next(v);
        nome=v.asString();
        r.next(v);
        catena=v.asString();
        r.next(v);
        idMotif=v.asString();
        r.next(v);
        catenaMotif=v.asString();
        r.next(v);
        id=v.asString();
        r.next(v);
        numberItems=v.asString();


        std::cout<<"Motivo ora in esecuzione "<<nome<<"|" <<catena<<"|"<<id<<std::endl;

        query2="for $hit in collection(\"containerAnalisi.dbxml\")/test/motif/proteina \n";
        query2+=" where not (data($hit/@name)=\""+nome+"\" ";
        query2+=" and data($hit/@catenaSearch)=\""+catena+"\"";
        query2+=" and data($hit/@idSearch)=\""+id+"\") ";
        foreach(candidato* current,parziale){
            query2+=" and not (data($hit/@name)=\""+current->proteina+"\" ";
            query2+=" and data($hit/@catenaSearch)=\""+current->catena+"\"";
            query2+=" and data($hit/@idSearch)=\""+current->id+"\") ";
        }
        query2+=" return \n ";
        query2+=" (data($hit/../@catenaSource), data($hit/../@idSource),data($hit/@name), data($hit/@catenaSearch), data($hit/@idSearch))" ;

        r2=doFlowrQuery(query2);

        //cout<<"***CIAO \n "+query2<<endl;
        for(int i=0;i<r2.size()/5;i++){
            candidatoPar=new candidato;
            r2.next(v2);
            candidatoPar->catenaRif=v2.asString();
            r2.next(v2);
            candidatoPar->idRif=v2.asString();
            r2.next(v2);
            candidatoPar->proteina=v2.asString();
            r2.next(v2);
            candidatoPar->catena=v2.asString();
            r2.next(v2);
            candidatoPar->id=v2.asString();

            auto checkEquals= [parziale,candidatoPar]()->bool {
                foreach (candidato* current, parziale) {
                    if(current->proteina.compare(candidatoPar->proteina)!=0 &&
                            current->catena.compare(candidatoPar->catena)!=0 &&
                            current->proteina.compare(candidatoPar->proteina)!=0)
                        return true;
                     }
                return false;
            };


            if(!checkEquals())
                parziale.push_back(candidatoPar);
            //std::cout<<"***CIAO "<<parziale.size()<<candidatoPar->proteina<<candidatoPar->catena<<candidatoPar->id<<std::endl;
        }

        query2=" declare namespace functx=\"http://www.functx.com\"; \n";
        query2+=" declare function functx:mysort ( $seq as item()* ) as item()* {for $item in $seq order by number($item) return $item} ; \n ";
        query2+=" for $pMotif in collection(\""+container.getName()+"\")/search-result/search-result-chains/search-result-chain/search-hits/search-hit";
        query2+=" let $p:= $pMotif/../../../.. \n";
        query2+=" where ($p/search-protein/@name=\""+candidatoPar->proteina+"\" and $p/source-protein/@name=\""+nome+"\" \n";
        query2+=" and $pMotif/@source-chain=\""+catena+"\" and $pMotif/@search-chain=\""+candidatoPar->catena+"\" ";
        query2+=" and string-join(distinct-values(data($pMotif/source-motif/ss-index/@id)),' ')=\""+id+"\"" ;
        query2+=" and string-join(functx:mysort(distinct-values(data(($pMotif/peak/vote/@ss1-id,$pMotif/peak/vote/@ss2-id)))),' ')=\""+candidatoPar->id+"\" ";
        for (int i = 0; i < avoided.size(); ++i) {
            query2+=" and $p/source-protein/@name!=\""+avoided[i]+"\" and $p/search-protein/@name!=\""+avoided[i]+"\" ";
        }
        query2+=")";
        query2+=" \n return ";
        query2+= " <result catenaA=\""+catenaMotif+"\" catenaB=\""+candidatoPar->catenaRif+"\" idA=\""+idMotif+"\" numberItems=\""+numberItems+"\" idB=\""+candidatoPar->idRif+"\">\n";
        query2+="\t\n<confirm sourceProtein=\"{data($p/source-protein/@name)}\" searchProtein=\"{data($p/search-protein/@name)}\" catenaSource=\""+catena+"\" idSource=\""+id+"\" catenaSearch=\""+candidatoPar->catena+"\" idSearch=\""+candidatoPar->id+"\"/>\n";
        query2+="</result>  \n";

        /*
            query2=" declare namespace functx=\"http://www.functx.com\"; \n";
            query2+=" declare function functx:mysort ( $seq as item()* ) as item()* {for $item in $seq order by number($item) return $item} ; \n ";
            query2+="for $hit in collection(\"containerAnalisi.dbxml\")/test/motif/proteina \n";
            //        query2+=" let $hitM:=$hit/.. ";
            //query2+=" order by data($hit/../@catenaSource), data($hit/../@idSource),data($hit/@name),data($hit/@catenaSearch), data($hit/@idSearch) ";
            query2+=" where not(data($hit/@name)=\""+nome+"\" ";
            query2+=" and data($hit/@catenaSearch)=\""+catena+"\"";
            query2+=" and data($hit/@idSearch)=\""+id+"\" ";
            //for (int i = 0; i < avoided.size(); ++i) {
            //    query2+=" and $hit/@name!=\""+avoided[i]+"\"";
            //}
            query2+=")";
            query2+=" return \n ";
            //std::cout<<"ora uso "<<container.getName()<<std::endl;
            query2+=" for $pMotif in collection(\""+container.getName()+"\")/search-result/search-result-chains/search-result-chain/search-hits/search-hit";
            query2+=" let $p:= $pMotif/../../../.. \n";
            query2+=" where ($p/search-protein/@name=$hit/@name and $p/source-protein/@name=\""+nome+"\" \n";
            query2+=" and $pMotif/@source-chain=\""+catena+"\" and $pMotif/@search-chain=$hit/@catenaSearch ";
            query2+=" and string-join(distinct-values(data($pMotif/source-motif/ss-index/@id)),' ')=\""+id+"\"" ;
            query2+=" and string-join(functx:mysort(distinct-values(data(($pMotif/peak/vote/@ss1-id,$pMotif/peak/vote/@ss2-id)))),' ')=data($hit/@idSearch)) ";
            //query2+=" or ($p/source-protein/@name=$hit/@name and $p/search-protein/@name=\""+nome+"\" \n";
            //query2+=" and $pMotif/@search-chain=\""+catena+"\" and $pMotif/@source-chain=$hit/@catenaSearch ";
            //query2+=" and string-join(distinct-values(data($pMotif/source-motif/ss-index/@id)),' ')=data($hit/@idSearch) " ;
            //query2+=" and string-join(functx:mysort(distinct-values(data(($pMotif/peak/vote/@ss1-id,$pMotif/peak/vote/@ss2-id)))),' ')=\""+id+"\" )) ";
            //for (int i = 0; i < avoided.size(); ++i) {
            //    query2+=" and $p/source-protein/@name!=\""+avoided[i]+"\" and $p/search-protein/@name!=\""+avoided[i]+"\"";
            //}
            query2+=" \n return ";
            query2+=" if ($p/source-protein/@name=\""+nome+"\") ";
            query2+= " then <result catenaA=\""+catenaMotif+"\" catenaB=\"{data($hit/../@catenaSource)}\" idA=\""+idMotif+"\" numberItems=\""+numberItems+"\" idB=\"{data($hit/../@idSource)}\">\n";
            query2+="\t\n<confirm sourceProtein=\"{data($p/source-protein/@name)}\" searchProtein=\"{data($p/search-protein/@name)}\" catenaSource=\""+catena+"\" idSource=\""+id+"\" catenaSearch=\"{data($hit/@catenaSearch)}\" idSearch=\"{data($hit/@idSearch)}\"/>\n";
            query2+="</result>  \n";
            query2+= " else <result catenaA=\"{data($hit/../@catenaSource)}\" catenaB=\""+catenaMotif+"\" idA=\"{data($hit/../@idSource)}\" numberItems=\""+numberItems+"\" idB=\""+idMotif+"\">\n";
            query2+="\t\n<confirm sourceProtein=\"{data($p/source-protein/@name)}\" searchProtein=\"{data($p/search-protein/@name)}\" catenaSource=\"{data($hit/@catenaSearch)}\" idSource=\"{data($hit/@idSearch)}\" catenaSearch=\""+catena+"\" idSearch=\""+id+"\"/>\n";
            query2+="</result>  \n";

            */
        r2=doFlowrQuery(query2);

        /*std::ofstream fil("query");
        fil << query2 << std::endl;
        fil.close();*/

        if(std::find(avoided.begin(), avoided.end(), nome) == avoided.end())
            avoided.push_back(nome);
        //    std::cout<<"Nuovo eliminato: "+nome+"\n******\n"<<std::endl;
        //ax+="Nuovo eliminato: "+nome+"\n******\n";
        //printf("%d \n",r2.size());
        //ax+="ESEGUO LA QUERY: \n\n"+query2+"\n\n\n\n";
        while(r2.next(v2)){

            //     std::cout<<v2.asString()<<std::endl;
            ax+="\n\t"+v2.asString();
        }


    }



    ax+="\n</final>";
	if(!complete){
		ld.setContainer("risultati.dbxml");
	}else{
		ld.setContainer("SearchAllResult.dbxml");
	}
	ld.loadFromString("finalResult_"+nomeProteina,ax);
    ld.container.close();
    std::string fileName2="fine"+nomeProteina+".txt";

    std::cout<<"End evaluation"<<std::endl;
    std::ofstream file(fileName2.c_str());
    file << ax << std::endl;
    file.close();
}



