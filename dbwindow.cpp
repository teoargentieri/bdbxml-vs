#include "dbwindow.h"
#include "ui_dbwindow.h"


DbWindow::DbWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DbWindow),
    cathegoryValue(1)
{
    ui->setupUi(this);

    combobox=ui->comboBox;
    combobox->setItemData(0,1);
    combobox->setItemData(1,2);
    combobox->setItemData(2,3);

    label=ui->label;
    psLabel=ui->psLabel;
    psLabelDescription=ui->psLabelDescription;
    listWidget=ui->listWidget;
    startButton=ui->startButton;
    viewArea=ui->viewArea;
    viewArea->acceptRichText();
    loader=Loader("ContainerComparison.dbxml");
    queryManager=QueryManager("ContainerComparison.dbxml");
    queryManager.startingContainer="ContainerComparison.dbxml";
	queryManager.complete=false;
    mostranomi();

}

DbWindow::~DbWindow()
{
    delete ui;
}
void DbWindow::mostranomi(){
    listWidget->clear();
    std::string query="for $a in collection('" + queryManager.container.getName() + "') return dbxml:metadata(\"dbxml:name\", $a)";
    currentViewContainer=queryManager.container.getName();
    DbXml::XmlResults presenti=queryManager.doFlowrQuery(query);
    DbXml::XmlValue value;
    QString item;

    std::cout<<presenti.size()<<" report caricati"<<std::endl;
    while (presenti.hasNext()) {
        presenti.next(value);
        item=QString::fromStdString(value.asString());
        listWidget->addItem(item);
        //std::cout<<value.asString()+"\n";
    }
    listWidget->repaint();
}

void DbWindow::on_actionChoose_directory_triggered()
{

    QSettings settings;
    QString start_path = settings.value("start-path", QDir::homePath()).toString();
    QString filename=QFileDialog::getExistingDirectory(this,tr("Open File"),start_path,QFileDialog::ShowDirsOnly);
    std::cout<<filename.toStdString()<<std::endl;
    loader.loadDataFromDirectory(filename.toStdString().c_str());
    mostranomi();
}

void DbWindow::on_actionChoose_document_triggered()
{

    QSettings settings;
    QString start_path = settings.value("start-path", QDir::homePath()).toString();
    QString filename=QFileDialog::getOpenFileName(this,tr("Open File"),start_path,"XML Files(*.xml)");
    loader.loadDataFromFile(filename.toStdString().c_str());
    std::cout<<filename.toStdString()<<std::endl;
    mostranomi();
}


void DbWindow::on_DbWindow_iconSizeChanged(const QSize &iconSize)
{

}


void DbWindow::on_actionSet_Container_triggered()
{
    loader.container.close();
    queryManager.container.close();
    QSettings settings;
    QString start_path = settings.value("start-path", QDir::currentPath()).toString();
    QString filename=QFileDialog::getOpenFileName(this,tr("Open File"),start_path,"DBXML Files(*.dbxml)");
    std::string nomeContainer=filename.toStdString().substr(filename.toStdString().find_last_of("/")+1);
    loader.setContainer(nomeContainer);
    queryManager.setContainer(nomeContainer);
    queryManager.startingContainer=nomeContainer;
    mostranomi();
    std::cout<<"caricato il container: "+filename.toStdString();

}

void DbWindow::on_listWidget_doubleClicked(const QModelIndex &index)
{
    queryManager.setContainer(currentViewContainer);
    std::string view=queryManager.doGetDocument(listWidget->currentItem()->text().toStdString());
    viewArea->setText(QString::fromStdString(view));

}

void DbWindow::on_actionCreate_Container_triggered()
{
    std::cout<<"1\n";
    cr= new CreateContainer();
    std::cout<<"2\n";
    cr->setVisible(true);
    connect(cr,SIGNAL(mySignal()),this,SLOT(createContainerAction()),Qt::DirectConnection);

}
void DbWindow::createContainerAction(){


    loader.setContainer(cr->nuovoContainer);
    queryManager.setContainer(cr->nuovoContainer);
    mostranomi();
    std::cout<<"caricato il container: "+cr->nuovoContainer;
    cr->setDisabled(true);
    this->repaint();
}

void DbWindow::on_listWidget_clicked(const QModelIndex &index)
{
    std::string currentSource;
    currentSource=listWidget->currentItem()->text().toStdString();
    currentSource=currentSource.substr(0,currentSource.find_first_of("_"));
    psLabelDescription->setEnabled(true);
    psLabel->setText(QString::fromStdString(currentSource));
    startButton->setEnabled(true);
}

void DbWindow::on_startButton_clicked()
{
    std::string currentSource;
    currentSource=psLabel->text().toStdString();
    queryManager.crossSearch(currentSource);
    queryManager.setContainer("risultati.dbxml");
    newTreeView(currentSource);
}

void DbWindow::newTreeView(std::string currentSource){

    currentSource="Analisi risultati : "+currentSource;

    TreeModel* model=new TreeModel("risultati.dbxml",cathegoryValue,currentSource);
    TreeResults* tr=new TreeResults;
    tr->setModelView(model);
    tr->show();
    tr->setWindowTitle(QObject::tr(currentSource.c_str()));

}


void DbWindow::on_comboBox_activated(const QString &arg1)
{
    cathegoryValue=combobox->itemData(combobox->currentIndex()).toInt();
    std::cout<<cathegoryValue<<std::endl;
}


void DbWindow::on_actionSource_proteins_triggered()
{
    std::string query="for $a in collection('" + queryManager.container.getName() + "') return data($a/search-result/source-protein/@name)";
    DbXml::XmlResults presenti=queryManager.doFlowrQuery(query);
    DbXml::XmlValue value;
	QList<string> pSorgenti;
	while (presenti.hasNext()) {
        presenti.next(value);
		if(!pSorgenti.contains(value.asString())){
		pSorgenti.push_back(value.asString());
		cout<<value.asString()<<endl;
		}
		
    }
	cout<<"Source proteins: "<<pSorgenti.size()<<endl;

	    query="for $a in collection('" + queryManager.container.getName() + "') return data($a/search-result/@found-motifs)";
   presenti=queryManager.doFlowrQuery(query);
    
	int nMotif=0;
	while (presenti.hasNext()) {
        presenti.next(value);
		nMotif+=value.asNumber();
	}
	
	cout<<"Candidate motifs: "<<nMotif<<endl;
}

void DbWindow::on_actionSerach_all_triggered(){
	queryManager.complete=true;
	string query="for $a in collection('" + queryManager.container.getName() + "') return data($a/search-result/source-protein/@name)";
    DbXml::XmlResults presenti=queryManager.doFlowrQuery(query);
    DbXml::XmlValue value;
	QList<string> pSorgenti;
	while (presenti.hasNext()) {
        presenti.next(value);
		if(!pSorgenti.contains(value.asString())){
		pSorgenti.push_back(value.asString());
		cout<<"Current Protein "<<value.asString()<<endl;
		queryManager.crossSearch(value.asString());
		}
}
		queryManager.setContainer("SearchAllResult.dbxml");
		query="for $a in collection('" + queryManager.container.getName() + "')/final return (data($a/@protein),count(data($a/result)))";

		presenti=queryManager.doFlowrQuery(query);
		int totalCount=0;
		while (presenti.hasNext()) {
	
			presenti.next(value);
			cout<<"Protein in final"<<value.asString()<<endl;
			presenti.next(value);
			cout<<"Motif Found "<<value.asString()<<endl;
			totalCount+=value.asNumber();
		}	
}

void DbWindow::on_actionClear_previous_research_triggered()
{
	if(queryManager.exists_test("SearchAllResult.dbxml"))
		remove("SearchAllResult.dbxml");
}
