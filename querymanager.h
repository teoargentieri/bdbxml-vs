#ifndef QUERYMANAGER_H
#define QUERYMANAGER_H
#include <dbxml/DbXml.hpp>
#include <db.h>
#include <sys/stat.h>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;
class QueryManager
{
public:
    QueryManager();
    QueryManager(const char* containerName);

    //attributes
    DbXml::XmlManager manager;
    DbXml::XmlContainer container;
    std::string startingContainer;
	bool complete;

    struct candidato{

        string idRif;
        string catenaRif;
        string id;
        string catena;
        string proteina;
    };

    //metods
    std::string doGetDocument(const std::string docname);
    std::string doQuery(const std::string &fullQuery);
    void setContainer(std::string containerName);
    DbXml::XmlResults doFlowrQuery(const std::string &query);
    DbXml::XmlResults doFlowrQuery(const std::string &query,DbXml::XmlQueryContext context);
    DbXml::XmlResults ricercaCiclo(std::string nomeProteina);
    inline bool exists_test (const std::string& name);
    void crossSearch(std::string nomeProteina);

};

#endif // QUERYMANAGER_H
