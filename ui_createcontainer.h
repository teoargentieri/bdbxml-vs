/********************************************************************************
** Form generated from reading UI file 'createcontainer.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CREATECONTAINER_H
#define UI_CREATECONTAINER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CreateContainer
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPlainTextEdit *plainTextEdit;
    QLabel *label;
    QLabel *label_2;
    QPushButton *pushButton;

    void setupUi(QDialog *CreateContainer)
    {
        if (CreateContainer->objectName().isEmpty())
            CreateContainer->setObjectName(QStringLiteral("CreateContainer"));
        CreateContainer->resize(400, 142);
        buttonBox = new QDialogButtonBox(CreateContainer);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(50, 100, 341, 51));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        horizontalLayoutWidget = new QWidget(CreateContainer);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(0, 48, 391, 31));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        plainTextEdit = new QPlainTextEdit(horizontalLayoutWidget);
        plainTextEdit->setObjectName(QStringLiteral("plainTextEdit"));

        horizontalLayout->addWidget(plainTextEdit);

        label = new QLabel(horizontalLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        label_2 = new QLabel(CreateContainer);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 10, 141, 17));
        pushButton = new QPushButton(CreateContainer);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(10, 110, 99, 27));

        retranslateUi(CreateContainer);
        QObject::connect(buttonBox, SIGNAL(accepted()), CreateContainer, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), CreateContainer, SLOT(reject()));

        QMetaObject::connectSlotsByName(CreateContainer);
    } // setupUi

    void retranslateUi(QDialog *CreateContainer)
    {
        CreateContainer->setWindowTitle(QApplication::translate("CreateContainer", "Dialog", 0));
        label->setText(QApplication::translate("CreateContainer", ".dbxml", 0));
        label_2->setText(QApplication::translate("CreateContainer", "Nuovo container", 0));
        pushButton->setText(QApplication::translate("CreateContainer", "Close", 0));
    } // retranslateUi

};

namespace Ui {
    class CreateContainer: public Ui_CreateContainer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CREATECONTAINER_H
