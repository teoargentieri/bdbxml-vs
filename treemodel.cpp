
/*
    treemodel.cpp

    Provides a simple tree model to show how to create and use hierarchical
    models.
*/

#include "treeitem.h"
#include "treemodel.h"

#include <QStringList>

using namespace DbXml;
using namespace std;

TreeModel::TreeModel(std::string nomeContainer, int cathegoryValue,std::string protein, QObject *parent)
    : QAbstractItemModel(parent)
{
    QList<QVariant> rootData;
    rootData << "Motifs" << "Count"<<"Related Motif"<<"Number of Items"<<"Score";
    rootItem = new TreeItem(rootData);

    this->cathegoryValue=cathegoryValue;
    this->protein=protein;
    setupModelData(nomeContainer,rootItem);
}

TreeModel::~TreeModel()
{
    delete rootItem;
}

int TreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());

    return item->data(index.column());
}

Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return QAbstractItemModel::flags(index);
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent)
const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parentItem();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}


TreeItem* TreeModel::getRoot()
{
    return rootItem;
}
void TreeModel::setupModelData(std::string nomeContainer, TreeItem *parent)
{
    TreeItem* motif;
    QList<QVariant> items,comparison;
    std::vector<std::string> ones;
    std::vector<std::string> related;
    //istanzio le qlist con valori casuali per evitare un indexOutOfBounds
    comparison<<0;
    items<<0<<1<<2<<3<<4;

    XmlValue v,v2;
    XmlResults r,r2;
    QueryManager qm=QueryManager();
    qm.setContainer(nomeContainer);
    std::string query,numberItems,query2,relatedAll="";

    query="for $a in collection(\""+qm.container.getName()+"\")/final/result";
    query+=" return (data($a/@numberItems),distinct-values((string-join(data(($a/@catenaA,$a/@idA)),' | '),string-join(data(($a/@catenaB,$a/@idB)),' | '))))";

    r=qm.doFlowrQuery(query);

    while (r.next(v)) {

        //guardo se si tratta dell'id oppure del numero di elementi
        if(v.asString().find("|")==std::string::npos){
            numberItems=v.asString();
            r.next(v);
        }

        if(std::find(ones.begin(), ones.end(), v.asString()) == ones.end()) {
            ones.push_back(v.asString());
            items[0]=QString::fromStdString(v.asString());
            file.append((v.asString()+"\n\n").c_str());

            query2="for $a in collection (\""+qm.container.getName()+"\")/final/result \n ";
            query2+="where $a/@idA!=$a/@idB and $a/@catenaA!=$a/@catenaB \n";
            query2+=" return \n if (data($a/@catenaA)=\""+v.asString().substr(0,1)+"\" and data($a/@idA)=\""+v.asString().substr(4,v.asString().length()-1)+"\")";
            query2+=" \n then (distinct-values(string-join(data(($a/@catenaB,$a/@idB)),' | '))) ";
            query2+=" else if (data($a/@catenaB)=\""+v.asString().substr(0,1)+"\" and data($a/@idB)=\""+v.asString().substr(4,v.asString().length()-1)+"\") ";
            query2+=" \n then (distinct-values(string-join(data(($a/@catenaA,$a/@idA)),' | '))) ";
            query2+=" else '0'";
            r2=qm.doFlowrQuery(query2);
            relatedAll="";
            related.clear();
            while(r2.next(v2)){
                if(v2.asString().compare("0")!=0 && (std::find(related.begin(), related.end(), v2.asString()) == related.end())){
                    related.push_back(v2.asString());
                    relatedAll+=" "+ v2.asString()+" ; ";

                }
            }


            query2="for $a in collection (\""+qm.container.getName()+"\")/final/result \n ";
            query2+="where $a/@idA=\""+v.asString().substr(4,v.asString().length()-1)+"\" and $a/@catenaA=\""+v.asString().substr(0,1)+"\" or \n";
            query2+=" $a/@idB=\""+v.asString().substr(4,v.asString().length()-1)+"\" and $a/@catenaB=\""+v.asString().substr(0,1)+"\" ";
            query2+=" return string-join((string-join((data($a/confirm/@sourceProtein),data($a/confirm/@catenaSource),data($a/confirm/@idSource)),' | '),string-join((data($a/confirm/@searchProtein),data($a/confirm/@catenaSearch),data($a/confirm/@idSearch)),' | ')), ' con ')";

            r2=qm.doFlowrQuery(query2);

            //istanzio i vari attributi da associare al singolo elemento del modello
            items[1]=r2.size();
            items[2]=QString::fromStdString(relatedAll);
            items[3]=QString::fromStdString(numberItems);
            double score=(stoi(numberItems)-3)+(static_cast<double>(r2.size()))*0.01*cathegoryValue;
            items[4]=QString::number(score,'g',6);

            motif=new TreeItem(items,parent);
            parent->appendChild(motif);

            file.append(("Motif: "+items[0].toString().toStdString()+" Score: " +items[4].toString().toStdString()+"\n").c_str());
            file.append(("Number of items: "+numberItems+" Related: " +relatedAll+"\n").c_str());
            file.append("\n Matches: \n");
            while(r2.next(v2)){
                comparison[0]=QString::fromStdString(v2.asString());
                motif->appendChild(new TreeItem(comparison,motif));
                file.append((comparison[0].toString().toStdString()+"\n").c_str());
            }
            file.append("\n");

        }

    }


}
